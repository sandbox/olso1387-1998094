<?php
/**
 * @file theme-settings.php
 *
 * Adds optional theme settings.
 */

/**
 * Implements HOOK_form_FORM_ID_alter().
 * Here is a list of settings added to the form in this function.
 *    normal_links_color
 *    normal_links_bold
 *    normal_links_underline
 *    default_active_links
 *    active_links_color
 *    active_links_bold
 *    active_links_underline
 *    default_hover_links
 *    hover_links_color
 *    hover_links_bold
 *    hover_links_underline
 *    default_header_image
 *    header_image_path
 *    header_background_color
 *    default_footer_image
 *    footer_image_path
 *    footer_background_color
 *    htabs_background_color
 *    tab_background_color
 *    active_tab_background_color
 *    htabs_separator_color
 *    h1_font_family
 *    h1_font_size
 *    h1_font_weight
 *    h2_font_family
 *    h2_font_size
 *    h2_font_weight
 *    h3_font_family
 *    h3_font_size
 *    h3_font_weight
 *    content_font_family
 *    content_font_size
 *    content_font_weight
 *
 * @param $form
 *   array The form being altered.
 * @param $form_state
 *   array The current form state.
 * @return
 *   array A form array.
 */
function sun_dog_form_system_theme_settings_alter(&$form, &$form_state) {
  
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('theme', 'sun_dog') . '/theme-settings.php';
  
  $form['sun_dog_settings_title'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sun Dog Settings'),
  );
  
  $form['sun_dog_settings_title']['sun_dog_theme_settings'] = array(
    '#type' => 'vertical_tabs',
    '#title' => t('Sun Dog Theme Settings'),
  );
  
  /***************************************************************************/
  /* LINK SETTINGS                                                           */
  /***************************************************************************/

  // Link settings for normal, active, and hover links

  $form['sun_dog_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Links'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'sun_dog_theme_settings',
  );

  $form['sun_dog_links']['normal_links'] = array(
    '#type' =>'fieldset',
    '#title' => t('Normal Links'),
  );

  $form['sun_dog_links']['normal_links']['normal_links_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color for html links'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('normal_links_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a color for links.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  $form['sun_dog_links']['normal_links']['normal_links_bold'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bold normal links'),
    '#default_value' => theme_get_setting('normal_links_bold', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want links to be bold.'),
  );

  $form['sun_dog_links']['normal_links']['normal_links_underline'] = array(
    '#type' => 'checkbox',
    '#title' => t('Underline normal links'),
    '#default_value' => theme_get_setting('normal_links_underline', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want links to be underline.'),
  );

  $form['sun_dog_links']['active_links'] = array(
    '#type' =>'fieldset',
    '#title' => t('Active Links'),
  );

  $form['sun_dog_links']['active_links']['default_active_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use normal link settings for active links'),
    '#default_value' => theme_get_setting('default_active_links', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the normal link settings for the active link settings.'),
  );

  $form['sun_dog_links']['active_links']['settings'] = array(
    '#type' =>'container',
    '#states' => array(
      'visible' => array(
        'input[name="default_active_links"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['sun_dog_links']['active_links']['settings']['active_links_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color for active links'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('active_links_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a color for active links.'),
  );

  $form['sun_dog_links']['active_links']['settings']['active_links_bold'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bold active links'),
    '#default_value' => theme_get_setting('active_links_bold', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want active links to be bold.'),
  );

  $form['sun_dog_links']['active_links']['settings']['active_links_underline'] = array(
    '#type' => 'checkbox',
    '#title' => t('Underline active links'),
    '#default_value' => theme_get_setting('active_links_underline', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want active links to be underline.'),
  );

  $form['sun_dog_links']['hover_links'] = array(
    '#type' =>'fieldset',
    '#title' => t('Hover Links'),
  );

  $form['sun_dog_links']['hover_links']['default_hover_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use normal link settings for hover links'),
    '#default_value' => theme_get_setting('default_hover_links', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the normal link settings for the hover link settings.'),
  );

  $form['sun_dog_links']['hover_links']['settings'] = array(
    '#type' =>'container',
    '#states' => array(
      'visible' => array(
        'input[name="default_hover_links"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['sun_dog_links']['hover_links']['settings']['hover_links_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color for links when a mouse hovers over them.'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('hover_links_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a color for links when a mouse hovers over them.'),
  );

  $form['sun_dog_links']['hover_links']['settings']['hover_links_bold'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bold hover links'),
    '#default_value' => theme_get_setting('hover_links_bold', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want hover links to be bold.'),
  );

  $form['sun_dog_links']['hover_links']['settings']['hover_links_underline'] = array(
    '#type' => 'checkbox',
    '#title' => t('Underline hover links'),
    '#default_value' => theme_get_setting('hover_links_underline', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want hover links to be underline.'),
  );


  /***************************************************************************/
  // HEADER IMAGE
  /***************************************************************************/
  $form['sun_dog_header_image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header Image'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'sun_dog_theme_settings',
  );

  $form['sun_dog_header_image']['default_header_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default header image.'),
    '#default_value' => theme_get_setting('default_header_image', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the header image supplied with it.')
  );

  $form['sun_dog_header_image']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        'input[name="default_header_image"]' => array('checked' => FALSE),
      ),
    ),
  );
  
  $form['sun_dog_header_image']['settings']['header_image_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom header image'),
    '#description' => t('The path to the file you would like to use as your background header image instead of the background header image supplied with the theme.'),
    '#default_value' => theme_get_setting('header_image_path', 'sun_dog'),
  );

  $form['sun_dog_header_image']['settings']['header_image_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload header image'),
    '#maxlength' => 40,
    '#description' => t('If you don\'t have direct file access to the server, use this field to upload your background header image.')
  );

  $form['sun_dog_header_image']['header_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color that extends past the image'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('header_background_color', 'sun_dog'),
    '#description' => t('Define a color, using hex color codes (#000000 => Black), that will extend beyond the sides of the header image to the edge of the browser window.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  /***************************************************************************/
  // FOOTER IMAGE
  /***************************************************************************/
  $form['sun_dog_footer_image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer Image'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'sun_dog_theme_settings',
  );

  $form['sun_dog_footer_image']['default_footer_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default footer image.'),
    '#default_value' => theme_get_setting('default_footer_image', 'sun_dog'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the footer image supplied with it.')
  );

  $form['sun_dog_footer_image']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        'input[name="default_footer_image"]' => array('checked' => FALSE),
      ),
    ),
  );
  
  $form['sun_dog_footer_image']['settings']['footer_image_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom footer image'),
    '#description' => t('The path to the file you would like to use as your background footer image instead of the background footer image supplied with the theme.'),
    '#default_value' => theme_get_setting('footer_image_path', 'sun_dog'),
  );

  $form['sun_dog_footer_image']['settings']['footer_image_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload footer image'),
    '#maxlength' => 40,
    '#description' => t('If you don\'t have direct file access to the server, use this field to upload your background footer image.')
  );

  $form['sun_dog_footer_image']['footer_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color that extends past the image'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('footer_background_color', 'sun_dog'),
    '#description' => t('Define a color, using hex color codes (#000000 => Black), that will extend beyond the sides of the footer image to the edge of the browser window.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  /***************************************************************************/
  // HORIZONTAL TABS
  /***************************************************************************/
  $form['sun_dog_horizontal_tabs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Horizontal Tabs'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'sun_dog_theme_settings',
  );

  $form['sun_dog_horizontal_tabs']['htabs_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color for a group of horizontal tabs'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('htabs_background_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a background color for a group of horzontal tabs.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  $form['sun_dog_horizontal_tabs']['tab_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color for a horizontal tab that is not currently active'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('tab_background_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a background color for a horzontal tab that is not currently active.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  $form['sun_dog_horizontal_tabs']['active_tab_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color for a horizontal tab that is currently active'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('active_tab_background_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a background color for a horzontal tab that is currently active.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  $form['sun_dog_horizontal_tabs']['htabs_separator_color'] = array(
    '#type' => 'textfield',
    '#title' => t('The color of the horizontal tab separator'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 6,
    '#maxlength' => 6,
    '#field_prefix' => '#',
    '#default_value' => theme_get_setting('htabs_separator_color', 'sun_dog'),
    '#description' => t('Using hex color codes (#000000 => Black), define a color for a horzontal tab separator.'),
    '#element_validate' => array('_validate_hex_color'),
  );

  /***************************************************************************/
  // FONTS
  /***************************************************************************/

  // an array of font options used by this theme
  $fonts = array(
    'Arial,Tahoma,Verdana' => 'Arial,Tahoma,Verdana',
    'Impact,Charcoal,Sans-Serif' => 'Impact,Charcoal,Sans-Serif',
    '"Trebuchet MS",Helvetica,Sans-Serif' => '"Trebuchet MS",Helvetica,Sans-Serif',
  );

  $form['sun_dog_fonts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fonts'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'sun_dog_theme_settings',
  );

  // HEADING 1
  $form['sun_dog_fonts']['h1_font'] = array(
    '#type' => 'fieldset',
    '#title' => t('H1'),
  );

  $form['sun_dog_fonts']['h1_font']['h1_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#options' => $fonts,
    '#default_value' => theme_get_setting('h1_font_family', 'sun_dog'),
    '#description' => t('Define the font family for the Heading 1 element.'),
  );

  $form['sun_dog_fonts']['h1_font']['h1_font_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Size'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 12,
    '#maxlength' => 12,
    '#default_value' => theme_get_setting('h1_font_size', 'sun_dog'),
    '#description' => t('Define the font size for the Heading 1 element.'),
    '#element_validate' => array('_validate_font_size'),
  );

  $form['sun_dog_fonts']['h1_font']['h1_font_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Weight'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => theme_get_setting('h1_font_weight', 'sun_dog'),
    '#description' => t('Define the font size for the Heading 1 element.'),
    '#element_validate' => array('_validate_font_weight'),
  );

  // HEADING 2
  $form['sun_dog_fonts']['h2_font'] = array(
    '#type' => 'fieldset',
    '#title' => t('H2'),
  );

  $form['sun_dog_fonts']['h2_font']['h2_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#options' => $fonts,
    '#default_value' => theme_get_setting('h2_font_family', 'sun_dog'),
    '#description' => t('Define the font family for the Heading 2 element.'),
  );

  $form['sun_dog_fonts']['h2_font']['h2_font_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Size'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 12,
    '#maxlength' => 12,
    '#default_value' => theme_get_setting('h2_font_size', 'sun_dog'),
    '#description' => t('Define the font size for the Heading 2 element.'),
    '#element_validate' => array('_validate_font_size'),
  );

  $form['sun_dog_fonts']['h2_font']['h2_font_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Weight'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => theme_get_setting('h2_font_weight', 'sun_dog'),
    '#description' => t('Define the font size for the Heading 2 element.'),
    '#element_validate' => array('_validate_font_weight'),
  );

  // HEADING 3
  $form['sun_dog_fonts']['h3_font'] = array(
    '#type' => 'fieldset',
    '#title' => t('H3'),
  );

  $form['sun_dog_fonts']['h3_font']['h3_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#options' => $fonts,
    '#default_value' => theme_get_setting('h3_font_family', 'sun_dog'),
    '#description' => t('Define the font family for the Heading 3 element.'),
  );

  $form['sun_dog_fonts']['h3_font']['h3_font_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Size'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 12,
    '#maxlength' => 12,
    '#default_value' => theme_get_setting('h3_font_size', 'sun_dog'),
    '#description' => t('Define the font size for the Heading 3 element.'),
    '#element_validate' => array('_validate_font_size'),
  );

  $form['sun_dog_fonts']['h3_font']['h3_font_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Weight'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => theme_get_setting('h3_font_weight', 'sun_dog'),
    '#description' => t('Define the font size for the Heading 3 element.'),
    '#element_validate' => array('_validate_font_weight'),
  );

 // CONTENT
  $form['sun_dog_fonts']['content_font'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
  );

  $form['sun_dog_fonts']['content_font']['content_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#options' => $fonts,
    '#default_value' => theme_get_setting('content_font_family', 'sun_dog'),
    '#description' => t('Define the font family for the page content.'),
  );

  $form['sun_dog_fonts']['content_font']['content_font_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Size'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 12,
    '#maxlength' => 12,
    '#default_value' => theme_get_setting('content_font_size', 'sun_dog'),
    '#description' => t('Define the font size for page content.'),
    '#element_validate' => array('_validate_font_size'),
  );

  $form['sun_dog_fonts']['content_font']['content_font_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Weight'),
    '#attributes' => array('style' => array('width' => 'width:auto;')),
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => theme_get_setting('content_font_weight', 'sun_dog'),
    '#description' => t('Define the font size for page content.'),
    '#element_validate' => array('_validate_font_weight'),
  );

  // add readable header image path
  if (isset($form['sun_dog_header_image']['settings']['header_image_path'])) {
    $element = $form['sun_dog_header_image']['settings']['header_image_path'];

    // If path is a public:// URI, display the path relative to the
    // Files directory.  Stream wrappers are not end-user friendly
    $original_path = $element['#default_value'];
    $friendly_path = NULL;
    if (file_uri_scheme($original_path) == 'public') {
      $friendly_path = file_uri_target($original_path);
      $element['#default_value'] = $friendly_path;
    }
  }

  // add readable footer image path
  if (isset($form['sun_dog_footer_image']['settings']['footer_image_path'])) {
    $element = $form['sun_dog_footer_image']['settings']['footer_image_path'];

    // If path is a public:// URI, display the path relative to the
    // Files directory.  Stream wrappers are not end-user friendly
    $original_path = $element['#default_value'];
    $friendly_path = NULL;
    if (file_uri_scheme($original_path) == 'public') {
      $friendly_path = file_uri_target($original_path);
      $element['#default_value'] = $friendly_path;
    }
  }

  // add custom validation and submit functions
  $form['#validate'][] = 'sun_dog_settings_validate';
  $form['#submit'][] = 'sun_dog_settings_submit';

  // Return the form with the additional widgets added
  return $form;
}

/**
 * Validate user settings in the Sun Dog theme settings form.
 *
 * @param $form
 *   array The form being altered.
 * @param $form_state
 *   array The current form state.
 */
function sun_dog_settings_validate($form, &$form_state) {
  $validators = array('file_validate_is_image' => array());

  // validate the header image
  $file = file_save_upload('header_image_upload', $validators);
  if (isset($file)) {
    if ($file) {
      $form_state['values']['header_image_upload'] = $file;
    }
    else {
      form_set_error('header_image_upload', t('The header image could not be uploaded.'));
    }
  }

  // if user provided a path, verify that the file exists
  if ($form_state['values']['header_image_path']) {
    $path = _system_theme_settings_validate_path($form_state['values']['header_image_path']);
    if (!$path) {
      form_set_error('header_image_path', t('The custom header image path is invalid.'));
    }
  }

  // validate the footer image
  $file = file_save_upload('footer_image_upload', $validators);
  if (isset($file)) {
    if ($file) {
      $form_state['values']['footer_image_upload'] = $file;
    }
    else {
      form_set_error('footer_image_upload', t('The footer image could not be uploaded.'));
    }
  }

  // if user provided a path, verify that the file exists
  if ($form_state['values']['footer_image_path']) {
    $path = _system_theme_settings_validate_path($form_state['values']['footer_image_path']);
    if (!$path) {
      form_set_error('footer_image_path', t('The custom footer image path is invalid.'));
    }
  }

  if (!$form_state['values']['default_active_links'] && !_validate_hex_string($form_state['values']['active_links_color'], 6)) {
    form_set_error('active_links_color', t('Invalid hex value in field "Color for Active Links".'));
  }
  if (!$form_state['values']['default_hover_links'] && !_validate_hex_string($form_state['values']['hover_links_color'], 6)) {
    form_set_error('hover_links_color', t('Invalid hex value in field "Color for Hover Links".'));
  }
}

/**
 * Upload files and save any other settings.
 *
 * @param $form
 *   array The form being altered.
 * @param $form_state
 *   array The current form state.
 */
function sun_dog_settings_submit($form, &$form_state) {

  // save uploaded file
  if ($file = $form_state['values']['header_image_upload']) {
    unset($form_state['values']['header_image_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $form_state['values']['default_header_image'] = 0;
    $form_state['values']['header_image_path'] = $filename;
  }
  // store header image path
  if (!empty($form_state['values']['header_image_path'])) {
    $form_state['values']['header_image_path'] = _system_theme_settings_validate_path($form_state['values']['header_image_path']);
  }

  // save uploaded file
  if ($file = $form_state['values']['footer_image_upload']) {
    unset($form_state['values']['footer_image_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $form_state['values']['default_footer_image'] = 0;
    $form_state['values']['footer_image_path'] = $filename;
  }
  // store footer image path
  if (!empty($form_state['values']['footer_image_path'])) {
    $form_state['values']['footer_image_path'] = _system_theme_settings_validate_path($form_state['values']['footer_image_path']);
  }
}

/**
 * Validate a hex color string.
 *
 * @param $element
 *   array The form element being tested.
 * @param $form_state
 *   array The current state of the form.
 */
function _validate_hex_color($element, &$form_state) {
  if (!empty($element['#value']) && !_validate_hex_string($element['#value'], 6)) {
    form_error($element, t('Invalid hex value in field "%name".', array('%name' => $element['title'])));
  }
}

/**
 * Validate a font size.
 *
 * @param $element
 *   array The form element being tested.
 * @param $form_state
 *   array The current state of the form.
 */
function _validate_font_size($element, &$form_state) {
  if (!empty($element['#value']) && !preg_match('/^((xx-|x-|)small(er|)|(xx-|x-|)large(r|)|inherit|[0-9\.]{1,4}[ ]{0,1}(pt|px|cm|em|in|\%))$/i', $element['#value'])) {
    form_error($element, t('Invalid font size.'));
  }
}

/**
 * Validate a font size.
 *
 * @param $element
 *   array The form element being tested.
 * @param $form_state
 *   array The current state of the form.
 */
function _validate_font_weight($element, &$form_state) {
  if (!empty($element['#value']) && !preg_match('/^(normal|bold|bolder|lighter|inherit|[1-9]{1}[0]{2})$/i', $element['#value'])) {
    form_error($element, t('Invalid font weight.'));
  }
}


/**
 * Validates a hex string of a given length.
 *
 * @param $value
 *   string The hex string being evaluated.
 * @param $length
 *   int The expected length of the string.
 * @return
 *   boolean True of the input value is a hex string of given length.
 */
function _validate_hex_string($value, $length) {
  if (!is_string($value) || !is_numeric($length)) {
    return FALSE;
  }

  $pattern = '/^[0-9a-fA-F]{' . $length . '}$/';

  if (preg_match($pattern, $value)) {
    return TRUE;
  }

  return FALSE;
}
