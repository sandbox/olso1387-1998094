@CHARSET "UTF-8";

<?php
/**
 * Finds the average of the hexadecimal color values in a standar
 * 6 character web color.
 * 
 * @param $hex_color
 *   string A 6 character hexadecimal string representing a web color.
 * @return
 *   int The average of the 3 RGB hex values.
 */
function _average_color_value($hex_color) {
  if (!is_string($hex_color) || !preg_match('/^[0-9a-fA-F]{6}$/', $hex_color)) {
    return 255;
  }

  $total = 0;
  for ($i = 0; $i < strlen($hex_color); $i = $i + 2) {
    $total = $total + hexdec(substr($hex_color, $i, 2));
  }

  return (int) $total/3;
}
?>

a,
a:link{
  color: #<?php print theme_get_setting('normal_links_color', 'sun_dog'); ?>;

<?php if (theme_get_setting('normal_links_bold', 'sun_dog')) : ?>
  font-weight: bold;
<?php else : ?>
  font-weight: normal;
<?php endif; ?>

<?php if (theme_get_setting('normal_links_underline', 'sun_dog')) : ?>
  text-decoration: underline;
<?php else : ?>
  text-decoration: none;
<?php endif; ?>
}

<?php if (! theme_get_setting('default_active_links', 'sun_dog')) : ?>
a:active,
a.active{
  color: #<?php theme_get_setting('active_links_color', 'sun_dog'); ?>;

  <?php if (theme_get_setting('active_links_bold', 'sun_dog')) : ?>
  font-weight: bold;
  <?php else : ?>
  font-weight: normal;
  <?php endif; ?>

  <?php if (theme_get_setting('active_links_underline', 'sun_dog')) : ?>
  text-decoration: underline;
  <?php else : ?>
  text-decoration: none;
  <?php endif; ?>
}
<?php endif; ?>

<?php if (! theme_get_setting('default_hover_links', 'sun_dog')) : ?>
a:hover{
  color: #<?php theme_get_setting('hover_links_color', 'sun_dog'); ?>;

  <?php if (theme_get_setting('hover_links_bold', 'sun_dog')) : ?>
  font-weight: bold;
  <?php else : ?>
  font-weight: normal;
  <?php endif; ?>

  <?php if (theme_get_setting('hover_links_underline', 'sun_dog')) : ?>
  text-decoration: underline;
  <?php else : ?>
  text-decoration: none;
  <?php endif; ?>
}
<?php endif; ?>

#header{
<?php if (theme_get_setting('default_header_image', 'sun_dog')) : ?>
  background-image: url('<?php print base_path() . drupal_get_path('theme', 'sun_dog') . '/images/bg-pattern.gif'; ?>');
<?php else : ?>
  background-image: url('<?php print file_create_url(theme_get_setting('header_image_path', 'sun_dog')); ?>');
<?php endif; ?>
  background-color: #<?php print theme_get_setting('header_background_color', 'sun_dog'); ?>;
  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: center top;

<?php if (_average_color_value(theme_get_setting('header_background_color', 'sun_dog')) < 128) : ?>
  color: white;
<?php else : ?>
  color: black;
<?php endif; ?>
}

#footer{
<?php if (theme_get_setting('default_footer_image', 'sun_dog')) : ?>
  background-image: url('<?php print base_path() . drupal_get_path('theme', 'sun_dog') . '/images/bg-pattern.gif'; ?>');
<?php else : ?>
  background-image: url('<?php print file_create_url(theme_get_setting('footer_image_path', 'sun_dog')); ?>');
<?php endif; ?>
  background-color: #<?php print theme_get_setting('footer_background_color', 'sun_dog'); ?>;
  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: center top;

<?php if (_average_color_value(theme_get_setting('footer_background_color', 'sun_dog')) < 128) : ?>
  color: white;
<?php else : ?>
  color: black;
<?php endif; ?>
}

.horizontal-tabs ul.horizontal-tabs-list,
.tabs ul.tabs{
  border-color: #<?php print theme_get_setting('htabs_separator_color', 'sun_dog'); ?>;
  background-color: #<?php print theme_get_setting('htabs_background_color', 'sun_dog'); ?>;
}

.horizontal-tabs ul.horizontal-tabs-list li.horizontal-tab-button.selected,
.horizontal-tabs ul.horizontal-tabs-list li.horizontal-tab-button a:hover,
.tabs ul.tabs li.active,
.tabs ul.tabs li a:hover{
  background-color: #<?php print theme_get_setting('active_tab_background_color', 'sun_dog'); ?>;

<?php if (_average_color_value(theme_get_setting('active_tab_background_color', 'sun_dog')) < 128) : ?>
  color: white;
<?php else : ?>
  color: black;
<?php endif; ?>
}

.horizontal-tabs ul.horizontal-tabs-list li.horizontal-tab-button,
.tabs ul.tabs li{
  border-color: #<?php print theme_get_setting('htabs_separator_color', 'sun_dog'); ?>;
  background-color: #<?php print theme_get_setting('tab_background_color', 'sun_dog'); ?>;

<?php if (_average_color_value(theme_get_setting('tab_background_color', 'sun_dog')) < 128) : ?>
  color: white;
<?php else : ?>
  color: black;
<?php endif; ?>
}


h1{
  font-family: <?php print theme_get_setting('h1_font_family', 'sun_dog'); ?>;
  font-size: <?php print theme_get_setting('h1_font_size', 'sun_dog'); ?>;
  font-weight: <?php print theme_get_setting('h1_font_weight', 'sun_dog'); ?>;
}

h2{
  font-family: <?php print theme_get_setting('h2_font_family', 'sun_dog'); ?>;
  font-size: <?php print theme_get_setting('h2_font_size', 'sun_dog'); ?>;
  font-weight: <?php print theme_get_setting('h2_font_weight', 'sun_dog'); ?>;
}

h3{
  font-family: <?php print theme_get_setting('h3_font_family', 'sun_dog'); ?>;
  font-size: <?php print theme_get_setting('h3_font_size', 'sun_dog'); ?>;
  font-weight: <?php print theme_get_setting('h3_font_weight', 'sun_dog'); ?>;
}

body{
  font-family: <?php print theme_get_setting('content_font_family', 'sun_dog'); ?>;
  font-size: <?php print theme_get_setting('content_font_size', 'sun_dog'); ?>;
  font-weight: <?php print theme_get_setting('content_font_weight', 'sun_dog'); ?>;
}

