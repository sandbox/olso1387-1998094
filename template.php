<?php

/**
 * Implements template_preprocess_html().
 */
function sun_dog_preprocess_html(&$variables) {
  
  $settings = variable_get('theme_sun_dog_settings', '') ? variable_get('theme_sun_dog_settings', '') : array();
  
  $variables['classes_array'] = array();
  
  if (!empty($variables['page']['sidebar_first'])) {
    $variables['classes_array'][] = 'with-sidebar-first';
  }

  if (!empty($variables['page']['sidebar_last'])) {
    $variables['classes_array'][] = 'with-sidebar-last';
  }
  
  if (!empty($settings['toggle_slogan'])) {
    $variables['classes_array'][] = 'with-slogan';
  }

  // add the dynamic css
  ob_start();
  include('css/dynamic.css.php');
  $css = ob_get_contents();
  ob_end_clean();

  drupal_add_css($css, array('type' => 'inline', 'group' => CSS_THEME, 'every_page' => FALSE, 'weight' => -1,));
}

/**
 * Implements template_preprocess_block().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function sun_dog_preprocess_block(&$variables, $hook) {
  // Classes describing the position of the block within the region.
  if ($variables['block_id'] == 1) {
    $variables['classes_array'][] = 'first';
  }
  // The last_in_region property is set in zen_page_alter().
  if (isset($variables['block']->last_in_region)) {
    $variables['classes_array'][] = 'last';
  }
  $variables['classes_array'][] = $variables['block_zebra'];

  $variables['title_attributes_array']['class'][] = 'block-title';
}

/**
 * Implements template_preprocess_node().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
function sun_dog_preprocess_node(&$variables) {
  switch ($variables['type']) {
    case 'blog' :
      if (array_key_exists('field_blog_picture', $variables['content'])) {
        $variables['classes_array'][] = 'node-blog-has-picture';
      }
      $variables['title_classes'] = 'node-title';
      break;
  }
}

/**
 * Implements template_preprocess_page().
 *
 * @param $variables
 *   array Variables to pass to the theme template.
 */
function sun_dog_preprocess_page(&$variables) {
  if (!$GLOBALS['user']->uid) {
    $user_links = array(
      'links' => array(
        'login' => array(
          'title' => t('Login'),
          'href' => 'user/login',
        ),
        'register' => array(
          'title' => t('Register'),
          'href' => 'user/register',
        ),
      ),
    );
  }
  else {
    $user_links = array(
      'links' => array(
        'account' => array(
          'title' => t('My account'),
          'href' => 'user/' . $GLOBALS['user']->uid,
        ),
        'logout' => array(
          'title' => t('Logout'),
          'href' => 'user/logout',
        ),
      ),
    );
  }
  $variables['user_links'] = theme('links', $user_links);
  
  $variables['classes_array'][] = 'wrapper-body';
}
